#ifndef MANAGER_H
#define MANAGER_H

#include <QtNetwork>
#include <QObject>
#include "peermanager.h"
#include "databasemanager.h"

class Manager : public QObject
{
    Q_OBJECT
private:
    DataBaseManager *db;
    PeerManager *peer;
    QTcpServer *server;

    void addNewPeer();

public:
    Manager();
};

#endif // MANAGER_H
