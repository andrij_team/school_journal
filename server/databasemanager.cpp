#include "databasemanager.h"

DataBaseManager::DataBaseManager(QObject *parent) : QObject(parent)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("school_journal.sql");
    qDebug() << "db open status: " << db.open();
    query = new QSqlQuery(db);
    createTables();
}

void DataBaseManager::createTables()
{
    // Table role
    qDebug() << "\ncreate role table: "
             << query->exec("CREATE TABLE IF NOT EXISTS Role ("
                            "id             INTEGER     PRIMARY KEY     AUTOINCREMENT,"
                            "name           VARCHAR(64) UNIQUE          NOT NULL,"
                            "description    VARCHAR(64) NOT NULL);")
             << query->lastError();

    qDebug() << "insert to role table: "
             << query->exec("INSERT INTO Role (name, description)"
                            " VALUES "
                            "('admin', 'Адміністратор, оперує усіма функціями.'),"                      /// 1
                            "('principal', 'Директор, корегує дані школи.'),"                           /// 2
                            "('teacher', 'Вчитель, працює з навчальним процесом.'),"                    /// 3
                            "('student', 'Студент, виконує завдання та надає зворотній звязок.'),"      /// 4
                            "('parent', 'Батьки, слідкування за успішністю та навчальним процесом.'),"  /// 5
                            "('guest', 'Гість, перегляд основних сторінок сайту.');")                   /// 6
             << query->lastError();

    // Table user
    qDebug() << "\ncreate table user:"
             << query->exec("CREATE TABLE IF NOT EXISTS Users ("
                            "id           INTEGER       PRIMARY KEY   AUTOINCREMENT,"
                            "activation   INTEGER       NOT NULL      DEFAULT 0,"
                            "grant        INTEGER       NOT NULL      DEFAULT 6,"
                            "email        VARCHAR(64)   UNIQUE        NOT NULL,"
                            "login        VARCHAR(64)   UNIQUE        NOT NULL,"
                            "name         VARCHAR(64)   NOT NULL,"
                            "password     VARCHAR(64)   NOT NULL,"
                            "secret_word  VARCHAR(32)   NOT NULL,"
                            "phone        VARCHAR(16)   NOT NULL,"
                            "gender       CHARACTER     NOT NULL);")
             << query->lastError();
    qDebug() << "insert to user: "
             << query->exec("INSERT INTO Users (login, password, secret_word, grant, name, gender, phone, email)"
                            " VALUES "
                            "('log_prl', 'qwerty1', 'sw_prnt', 1, 'Акалян Кикабидзе Івановна', 'Ж', '0999999999', 'ddd5@mail.ru'),"
                            "('log_tch', 'qwerty2', 'sw_tch',  3, 'Іван Іванов Іванович',      'М', '0999999999', 'ddd2@mail.ru'),"
                            "('log_std', 'qwerty3', 'sw_std',  4, 'Ірина Іванова Івановна',    'Ж', '0999999999', 'ddd3@mail.ru'),"
                            "('log_prn', 'qwerty4', 'sw_prnt', 2, 'Акалян Кикабидзе Івановна', 'Ж', '0999999999', 'ddd4@mail.ru'),"
                            "('log_gst', 'qwerty5', 'sw_prnt', 5, 'Акалян Кикабидзе Івановна', 'Ж', '0999999999', 'ddd6@mail.ru');")
             << query->lastError();

    // Table student-parent
    qDebug() << "\ncreate student_parent: "
             << query->exec("CREATE TABLE IF NOT EXISTS Student_parent ("
                            "id_student     INTEGER     NOT NULL,"
                            "id_parent      INTEGER     NOT NULL);")
             << query->lastError();
    qDebug() << "insert to student_parent: "
             << query->exec("INSERT INTO Student_parent (id_student, id_parent)"
                            " VALUES "
                            "(3, 5);")
             << query->lastError();

    // Table group
    qDebug() << "\ncreate group: "
             << query->exec("CREATE TABLE IF NOT EXISTS Groups ("
                            "id             INTEGER     PRIMARY KEY     AUTOINCREMENT,"
                            "number         INTEGER     NOT NULL,"
                            "char           CHARACTER   NOT NULL,"
                            "year_start     INTEGER     NOT NULL,"
                            "year_end       INTEGER     NOT NULL);")
             << query->lastError();
    qDebug() << "insert to group: "
             << query->exec("INSERT INTO Groups (number, char, year_start, year_end)"
                            " VALUES "
                            "(11, 'A', 2008, 2018);")
             << query->lastError();

    // Table student-group
    qDebug() << "\ncreate table student_group: "
             << query->exec("CREATE TABLE IF NOT EXISTS Student_group ("
                            "id_student     INTEGER     NOT NULL,"
                            "id_group       INTEGER     NOT NULL);")
             << query->lastError();
    qDebug() << "insert to student_group: "
             << query->exec("INSERT INTO Student_group (id_student, id_group)"
                            " VALUES "
                            "(3, 1);")
             << query->lastError();

    // Table student-group
    qDebug() << "\ncreate table subject: "
             << query->exec("CREATE TABLE IF NOT EXISTS Subject ("
                            "id     INTEGER         PRIMARY KEY     AUTOINCREMENT,"
                            "name   VARCHAR(64)     NOT NULL);")
             << query->lastError();
    qDebug() << "insert to subject: "
             << query->exec("INSERT INTO Subject (name)"
                            " VALUES "
                            "('Зарубіжна література'),"
                            "('Математика'),"
                            "('Іноземна мова'),"
                            "('Креслення'),"
                            "('Фізкультура'),"
                            "('Українська мова'),"
                            "('Астрономія');")
             << query->lastError();

    // Table lecture-hall
    qDebug() << "\ncreate table lecture_hall: "
             << query->exec("CREATE TABLE IF NOT EXISTS Lecture_hall ("
                            "id         INTEGER         PRIMARY KEY     AUTOINCREMENT,"
                            "number     INTEGER         NOT NULL,"
                            "flour      INTEGER         NOT NULL,"
                            "capacity   INTEGER         NOT NULL,"
                            "name       VARCHAR(64)     NOT NULL);")
             << query->lastError();
    qDebug() << "insert to lecture_hall: "
             << query->exec("INSERT INTO Lecture_hall (number, name, flour, capacity)"
                            " VALUES "
                            "(100, 'Кабінет літератури',      1, 30),"
                            "(101, 'Кабінет математики',      1, 30),"
                            "(102, 'Кабінет іноземної мови',  1, 30),"
                            "(103, 'Кабінет креслення',       1, 30);")
             << query->lastError();

    // Table lesson-timeline
    qDebug() << "\ncreate table lesson_timeline: "
             << query->exec("CREATE TABLE IF NOT EXISTS Lesson_timeline ("
                            "id             INTEGER         PRIMARY KEY     AUTOINCREMENT,"
                            "time_start     TIME            NOT NULL,"
                            "time_end       TIME            NOT NULL);")
             << query->lastError();
    qDebug() << "insert to lesson_timeline: "
             << query->exec("INSERT INTO Lesson_timeline (time_start, time_end)"
                            " VALUES "
                            "('08:45:00', '09:30:00'),"
                            "('09:35:00', '10:20:00'),"
                            "('10:25:00', '11:10:00'),"
                            "('11:15:00', '12:00:00'),"
                            "('12:05:00', '12:50:00'),"
                            "('12:55:00', '13:40:00');")
             << query->lastError();

    // Table semestr
    qDebug() << "\ncreate table semester: "
             << query->exec("CREATE TABLE IF NOT EXISTS Semester ("
                            "id             INTEGER         PRIMARY KEY     AUTOINCREMENT,"
                            "date_start     DATE            NOT NULL,"
                            "date_end       DATE            NOT NULL);")
             << query->lastError();
    qDebug() << "insert to semester: "
             << query->exec("INSERT INTO Semester (date_start, date_end)"
                            " VALUES "
                            "('2017-09-01', '2017-12-30'),"
                            "('2018-01-14', '2018-06-29');")
             << query->lastError();

    // Table schedule
    qDebug() << "\ncreate table schedule: "
             << query->exec("CREATE TABLE IF NOT EXISTS Schedule ("
                            "id                 INTEGER         PRIMARY KEY     AUTOINCREMENT,"
                            "id_lesson_time     INTEGER         NOT NULL,"
                            "weekday            INTEGER         NOT NULL,"
                            "id_group           INTEGER         NOT NULL,"
                            "id_teacher         INTEGER         NOT NULL,"
                            "id_lecture_hall    INTEGER         NOT NULL,"
                            "id_semester        INTEGER         NOT NULL,"
                            "id_subject         INTEGER         NOT NULL,"
                            "home_task          VARCHAR(256)    DEFAULT '');")
             << query->lastError();
    qDebug() << "insert to schedule: "
             << query->exec("INSERT INTO Schedule (id_lesson_time, weekday, id_group,"
                            "id_teacher, id_lecture_hall, id_semester, id_subject, home_task)"
                            " VALUES "
                            "(1, 1, 1, 1, 1, 1, 1, 'home1'),"
                            "(1, 2, 1, 1, 1, 1, 5, 'home2');")
             << query->lastError();

    // Table performance
    qDebug() << "\ncreate table perfomance: "
             << query->exec("CREATE TABLE IF NOT EXISTS Perfomance ("
                            "id                 INTEGER         PRIMARY KEY     AUTOINCREMENT,"
                            "id_lesson          INTEGER         NOT NULL,"
                            "id_student         INTEGER         NOT NULL,"
                            "value              INTEGER         NOT NULL);")
             << query->lastError();
    qDebug() << "insert to performance: "
             << query->exec("INSERT INTO Perfomance (id_lesson, id_student, value)"
                            " VALUES "
                            "(1, 1, 4),"
                            "(2, 1, 5);")
             << query->lastError();
}
