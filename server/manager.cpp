#include "manager.h"

Manager::Manager()
{
    db = new DataBaseManager(this);
    peer = new PeerManager(this);
    server = new QTcpServer(this);
    server->listen(QHostAddress::Any, 1666);

    QObject::connect(server, &QTcpServer::newConnection,
                     this, &Manager::addNewPeer);
}

void Manager::addNewPeer()
{
    peer->addNewUser(server->nextPendingConnection());
}
