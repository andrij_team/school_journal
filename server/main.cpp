#include <QtCore/QCoreApplication>
#include "manager.h"

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    Manager manager;

    return app.exec();
}
