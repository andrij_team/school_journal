#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QtCore>
#include <QtSql>

class DataBaseManager : public QObject
{
    Q_OBJECT
private:
    QSqlDatabase db;
    QSqlQuery *query;

    inline void createTables();

public:
    explicit DataBaseManager(QObject *parent = 0);

signals:

public slots:
};

#endif // DATABASEMANAGER_H
