#ifndef PEERMANAGER_H
#define PEERMANAGER_H

#include <QObject>
#include "peer.h"

class PeerManager : public QObject
{
    Q_OBJECT
private:
    QList <Peer*> peerList;

public:
    explicit PeerManager(QObject *parent = 0);

    void addNewUser(QTcpSocket *socket);

signals:

public slots:
};

#endif // PEERMANAGER_H
