#ifndef PEER_H
#define PEER_H

#include <QtCore>
#include <QtNetwork>

class Peer : public QObject
{
    Q_OBJECT
private:
    QTcpSocket *socket;

    void readyRead();

public:
    Peer(QTcpSocket *socket, QObject *parent = nullptr);
};

#endif // PEER_H
