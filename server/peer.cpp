#include "peer.h"

Peer::Peer(QTcpSocket *socket, QObject *parent)
    :socket(socket), QObject(parent)
{
    qDebug() << "create new peer";
    QObject::connect(socket, &QTcpSocket::readyRead, this, &Peer::readyRead);
}

void Peer::readyRead()
{
    //
}
