#include "C++/clientmanager.h"

int main(int argc, char **argv){
    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon("://Images/icon.jpg"));

    ClientManager *manager = new ClientManager;
    manager->start();

    return app.exec();
}
