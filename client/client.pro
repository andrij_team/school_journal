QT += qml
QT += quick
QT += network

CONFIG += c++14
CONFIG += qtquickcompiler

TARGET = School_journal

SOURCES += \
    main.cpp \
    C++/clientmanager.cpp \
    C++/server.cpp

RESOURCES += \
    resources.qrc

OTHER_FILES += $$PWD/QML/*.qml

HEADERS += \
    C++/clientmanager.h \
    C++/server.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../sj/release/ -lsj
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../sj/debug/ -lsj
else:unix: LIBS += -L$$OUT_PWD/../sj/ -lsj

INCLUDEPATH += $$PWD/../sj
DEPENDPATH += $$PWD/../sj

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../sj/release/libsj.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../sj/debug/libsj.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../sj/release/sj.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../sj/debug/sj.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../sj/libsj.a
