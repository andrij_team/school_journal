#include "clientmanager.h"

ClientManager::ClientManager(QObject *parent) : QObject(parent)
{
    user = new User;
    view = new QQuickView(QUrl("qrc:/QML/main.qml"));
    view->rootContext()->setContextProperty("Qt_client", this);
    server = new Server(this);
    view->rootContext()->setContextProperty("Qt_server", server);
    QObject::connect(server, &Server::get_QML_Object, this, &ClientManager::findObjectChild);
    QObject::connect(server, &Server::setNewUser, this, &ClientManager::setNewUser);
}

void ClientManager::start()
{
    view->resize(300, 500);
    view->show();
}

void ClientManager::quit()
{
    QGuiApplication::quit();
}

QObject* ClientManager::findObjectChild(QString objectName)
{
    return view->rootObject()->findChild <QObject*> (objectName);
}

void ClientManager::setNewUser(User user)
{
    qDebug() << "32";
    *this->user = user;
}

bool ClientManager::isAuthorizate()
{
    return user->isLogin();
}
