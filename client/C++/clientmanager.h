#ifndef CLIENTMANAGER_H
#define CLIENTMANAGER_H

#include <QtCore>
#include <QtQml>
#include <QtQuick>
#include <QGuiApplication>

#include "server.h"

class ClientManager : public QObject
{
    Q_OBJECT
private:
    User *user;
    QQuickView *view;
    Server *server;
    QObject* findObjectChild(QString objectName);

public:
    explicit ClientManager(QObject *parent = 0);
    void start();
    void setNewUser(User user);

    Q_INVOKABLE void quit();
    Q_INVOKABLE void logout(){}
    Q_INVOKABLE void getAccountInfo(){}
    Q_INVOKABLE bool isAuthorizate();

signals:

public slots:
};

#endif // CLIENTMANAGER_H
