#ifndef SERVER_H
#define SERVER_H

#include "user.h"
#include "sj.h"
#include <QObject>
#include <QtNetwork>

class Server : public QObject
{
    Q_OBJECT
private:
    QTcpSocket *socket;
    bool isConnect;

    void readyRead();
    bool testConnection();

public:
    Server(QObject *parent = nullptr);
    void tryConnectToServer();

    bool serverStatus();

    Q_INVOKABLE void tryAuth(QString login, QString password);
    Q_INVOKABLE void tryRegistrate(QString email,    QString name, QString telephon,
                                   QString password, QString rPassword);
    Q_INVOKABLE void tryRestorePassword(QString email);
    Q_INVOKABLE void showNextDate(QDate date);
    Q_INVOKABLE void showPrevDate(QDate date);
    Q_INVOKABLE void showDate(QDate date);
    Q_INVOKABLE void getLessonsByDate(QDate date);

signals:
    QObject* get_QML_Object(QString objectName);
    void setNewUser(User user);
};

#endif // SERVER_H
