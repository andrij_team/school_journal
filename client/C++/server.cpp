#include "server.h"

Server::Server(QObject *parent) : QObject(parent), isConnect(false)
{
    socket = new QTcpSocket(this);
    QObject::connect(socket, &QTcpSocket::connected,    [=](){ isConnect = true;  qDebug() << "connect to server"; });
    QObject::connect(socket, &QTcpSocket::disconnected, [=](){ isConnect = false; qDebug() << "disconnect server"; });
    QObject::connect(socket, &QTcpSocket::readyRead, this, &Server::readyRead);
    tryConnectToServer();
}

void Server::tryConnectToServer()
{
    if (isConnect)
        return;
    QMetaObject::invokeMethod(get_QML_Object("resultWindow"),
                              "setResult",
                              Q_ARG(QVariant, false),
                              Q_ARG(QVariant, "Наразі з'єднання з сервером відсутнє, "
                                              "перевіряємо можливість приєднання до сервера."));
    socket->connectToHost(QHostAddress::LocalHost, 1666);
}

bool Server::serverStatus()
{
    return isConnect;
}

void Server::readyRead()
{
    QDataStream stream(socket);
    Sj::Request request;
    stream >> request;

    switch (request){
    case Sj::Request::authorizate: {
        bool isAccept;
        stream >> isAccept;
        if (isAccept){
            User user;
            stream >> user;
            user.login();
            emit setNewUser(user);
        }
        break;
    }
    case Sj::Request::registrate: {
        bool isAccept;
        QString email;
        stream >> isAccept >> email;
        if (isAccept)
            QMetaObject::invokeMethod(get_QML_Object("resultWindow"),
                                      "setResult",
                                      Q_ARG(QVariant, true),
                                      Q_ARG(QVariant, email + " успішно зареєстровано."));
        else {
            QString error;
            stream >> error;
            QMetaObject::invokeMethod(get_QML_Object("resultWindow"),
                                      "setResult",
                                      Q_ARG(QVariant, false),
                                      Q_ARG(QVariant, email + " не вдалось зареэстувати, " + error));
        }
        break;
    }
    case Sj::Request::restorePassword: {
        //
        break;
    }
    case Sj::Request::getAccountInfo: {
        User user;
        stream >> user;
        user.login();
        emit setNewUser(user);
        break;
    }
    case Sj::Request::getLessonByDate: {
        //
        break;
    }
    default:;
    }
}

void Server::tryAuth(QString login, QString password)
{
    qDebug() << "Server::tryAuth: " << login << password;
    if (testConnection()){
        QDataStream stream(socket);
        stream << Sj::Request::authorizate << login << password;
    }
}

void Server::tryRegistrate(QString email,    QString name, QString telephon,
                               QString password, QString rPassword)
{
    qDebug() << "Server::tryRegistrate: " << email << name << telephon << password << rPassword;
    if (testConnection()){
        QDataStream stream(socket);
        stream << Sj::Request::registrate << email << name << telephon << password << rPassword;
    }
}

void Server::tryRestorePassword(QString email)
{
    qDebug() << "Server::tryRestorePassword: " << email;
    if (testConnection()){
        QDataStream stream(socket);
        stream << Sj::Request::restorePassword << email;
    }
}

void Server::showNextDate(QDate date)
{
    QMetaObject::invokeMethod(get_QML_Object("timeTable"),
                              "updateCurrentDate",
                              Q_ARG(QVariant, date.addDays(1)));
}

void Server::showPrevDate(QDate date)
{
    QMetaObject::invokeMethod(get_QML_Object("timeTable"),
                              "updateCurrentDate",
                              Q_ARG(QVariant, date.addDays(-1)));
}

void Server::showDate(QDate date)
{
    QMetaObject::invokeMethod(get_QML_Object("timeTable"),
                              "updateCurrentDate",
                              Q_ARG(QVariant, date));
}

void Server::getLessonsByDate(QDate date)
{
    if (testConnection()){
        QDataStream stream(socket);
        stream << Sj::Request::getLessonByDate << date;
    }
}

bool Server::testConnection()
{
    if (isConnect)
        return true;
    tryConnectToServer();
    return false;
}
