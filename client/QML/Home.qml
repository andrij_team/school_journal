import QtQuick 2.5
import "qrc:/QML"

Rectangle {
    anchors.fill: parent

    onVisibleChanged: {
        if (visible === true){
            console.log(Qt_client.isAuthorizate());
            authWindow.visible = ! Qt_client.isAuthorizate();
            iconWindow.visible = ! Qt_client.isAuthorizate();
            accountInfoWindow.visible = Qt_client.isAuthorizate();
            regWindow.visible = false;
            restoreWindow.visible = false;
        }
    }

    Authorization {
        id: authWindow
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: size * 4
        color: maincolor

        onRegistrate: {
            accountInfoWindow.visible = false;
            authWindow.visible = false;
            iconWindow.visible = false;
            regWindow.visible = true;
            restoreWindow.visible = false;
        }

        onRestorePassword: {
            accountInfoWindow.visible = false;
            authWindow.visible = false;
            regWindow.visible = false;
            iconWindow.visible = true;
            restoreWindow.visible = true;
        }
    }

    Registration {
        id: regWindow
        anchors.fill: parent
        color: maincolor
        visible: false

        onCancel: {
            accountInfoWindow.visible = false;
            authWindow.visible = true;
            iconWindow.visible = true;
            regWindow.visible = false;
            restoreWindow.visible = false;
        }
    }

    RestorePassword {
        id: restoreWindow
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: size * 4
        color: maincolor
        visible: false

        onCancel: {
            accountInfoWindow.visible = false;
            authWindow.visible = true;
            iconWindow.visible = true;
            regWindow.visible = false;
            restoreWindow.visible = false;
        }
    }

    AccountInfoWindow {
        id: accountInfoWindow
        anchors.fill: parent
        color: maincolor
        visible: false
    }

    Rectangle {
        id: iconWindow
        color: maincolor
        height: parent.height - authWindow.height
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        Image {
            source: "qrc:/Images/icon_bird.png"
            anchors.centerIn: parent
            width: parent.height
            height: parent.height
        }
    }
}
