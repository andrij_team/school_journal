import QtQuick 2.5

Rectangle {
    anchors.fill: parent
    color: maincolor

    ListModel {
        id: personalMessageModel

        ListElement {
            text: "Антон"
        }
        ListElement {
            text: "Версавий"
        }
        ListElement {
            text: "Тит"
        }
        ListElement {
            text: "Марк"
        }
        ListElement {
            text: "Виталий"
        }
        ListElement {
            text: "Дариус"
        }
        ListElement {
            text: "Ян"
        }
        ListElement {
            text: "Сергей"
        }
        ListElement {
            text: "Николай"
        }
    }

    ListView {
        id: viewPersonalMessage

        anchors.margins: 0
        anchors.fill: parent
        spacing: 2
        model: personalMessageModel
        clip: true

        delegate: Item {
            id: personalMessageDelegate

            property var view: ListView.view
            property var isCurrent: ListView.isCurrentItem

            width: view.width
            height: size

            Rectangle {
                anchors.fill: parent

                Text {
                    anchors.fill: parent
                    color: maincolor
                    text: "%1%2".arg(model.text).arg(isCurrent ? " *" : "")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                MouseArea {
                    anchors.fill: parent
//                    onClicked: {
//                        //view.currentIndex = model.index
//                        menuButton.visible = false
//                        backButton.visible = true
//                        title.text = "Личные: " + text
//                        hideAll()
//                        dialog.visible = true
//                    }
                }
            }
        }
    }
}
