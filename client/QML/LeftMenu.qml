import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "qrc:/QML"

Rectangle {
    opacity: opacityValue
    function setHideColorToButton() {
        contactsButton.color = hidecolor
        homeButton.color = hidecolor
        messageButton.color = hidecolor
        settingsButton.color = hidecolor
        timetableButton.color = hidecolor
        exitButton.color = hidecolor
    }

    function selectCurrent(){
        if (lastWindowOpen == "home")
            homeButton.color = currentcolor
        else if (lastWindowOpen == "message")
            messageButton.color = currentcolor
        else if (lastWindowOpen == "contacts")
            contactsButton.color = currentcolor
        else if (lastWindowOpen == "timetable")
            timetableButton.color = currentcolor
        else if (lastWindowOpen == "settings")
            settingsButton.color = currentcolor
    }

    Rectangle {
        id: menuButtonRect
        color: hidecolor
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: size

        Rectangle {
            id: homeButton
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: size
            color: currentcolor

            Image {
                source: "qrc:/Images/home.png"
                sourceSize.width: parent.width * 2 / 3
                sourceSize.height: parent.height * 2 / 3
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hideAll()
                    home.visible = true
                    homeButton.color = currentcolor
                    //topPanel.setNewTitle("Головна")
                    lastWindowOpen = "home"
                }
            }
        }

        Rectangle {
            id: timetableButton
            anchors.top: homeButton.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: size
            color: hidecolor

            Image {
                source: "qrc:/Images/calendar.png"
                sourceSize.width: parent.width * 2 / 3
                sourceSize.height: parent.height * 2 / 3
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hideAll()
                    timeTable.visible = true
                    parent.color = currentcolor
                    topPanel.setNewTitle("Розклад")
                    lastWindowOpen = "timetable"
                }
            }
        }

        Rectangle {
            id: contactsButton
            anchors.top: timetableButton.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: size
            color: hidecolor

            Image {
                source: "qrc:/Images/account.png"
                sourceSize.width: parent.width * 2 / 3
                sourceSize.height: parent.height * 2 / 3
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hideAll()
                    contacts.visible = true
                    parent.color = currentcolor
                    topPanel.setNewTitle("Контакти")
                    lastWindowOpen = "contacts"
                }
            }
        }

        Rectangle {
            id: messageButton
            anchors.top: contactsButton.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: size
            color: hidecolor

            Image {
                source: "qrc:/Images/message.png"
                sourceSize.width: parent.width * 2 / 3
                sourceSize.height: parent.height * 2 / 3
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hideAll()
                    message.visible = true
                    parent.color = currentcolor
                    topPanel.setNewTitle("Повідомлення")
                    lastWindowOpen = "message"
                }
            }
        }

        Rectangle {
            id: settingsButton
            anchors.bottom: exitButton.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: size
            color: hidecolor

            Image {
                source: "qrc:/Images/settings.png"
                sourceSize.width: parent.width * 2 / 3
                sourceSize.height: parent.height * 2 / 3
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hideAll()
                    settings.visible = true
                    parent.color = currentcolor
                    topPanel.setNewTitle("Налаштування")
                    lastWindowOpen = "settings"
                }
            }
        }

        Rectangle {
            id: exitButton
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: size
            color: hidecolor

            Image {
                source: "qrc:/Images/power.png"
                sourceSize.width: parent.width * 2 / 3
                sourceSize.height: parent.height * 2 / 3
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: Qt_client.quit()
            }
        }
    }
}
