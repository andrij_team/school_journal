import QtQuick 2.0

Rectangle {
    property int marginHeight: 5
    property int marginWidth: 10
    property int sizeValue: (size * 4 - marginHeight) / 5 - 4
    signal cancel();
    signal registrateProfile(string email, string  name, string password, string telephon);

    onVisibleChanged: {
        if (visible === false)
            return;
        emailRegInput.focus = true;
        emailRegInput.clear();
        telephonRegInput.clear();
        nameRegInput.clear();
        passwordRegInput.clear();
        dpasswordRegInput.clear();
        topPanel.setNewTitle("Реєстрація");
    }

    Column {
        height: parent.height - 2 * marginHeight
        width:  parent.width  - 2 * marginWidth
        x: marginWidth
        y: marginHeight
        spacing: 2

        // email
        Text {
            height: sizeValue
            color: maintextcolor
            text: qsTr("Пошта:");
            verticalAlignment: Text.AlignVCenter
            font.pointSize: height / 3
        }
        Rectangle {
            height: sizeValue
            width: parent.width
            color: maincolor
            border.color: maintextcolor
            border.width: 1

            TextInput {
                id: emailRegInput
                anchors.fill: parent
                anchors.leftMargin: 5
                color: maintextcolor
                verticalAlignment: Text.AlignVCenter
                font.pointSize: height / 3
                wrapMode: TextInput.WrapAnywhere

                onAccepted: nameRegInput.focus = true;
            }
        }

        // name
        Text {
            height: sizeValue
            color: maintextcolor
            text: qsTr("Ім'я:");
            verticalAlignment: Text.AlignVCenter
            font.pointSize: height / 3
        }
        Rectangle {
            height: sizeValue
            width: parent.width
            color: maincolor
            border.color: maintextcolor
            border.width: 1

            TextInput {
                id: nameRegInput
                anchors.fill: parent
                anchors.leftMargin: 5
                color: maintextcolor
                verticalAlignment: Text.AlignVCenter
                font.pointSize: height / 3
                wrapMode: TextInput.WrapAnywhere

                onAccepted: telephonRegInput.focus = true;
            }
        }

        // telephon
        Text {
            height: sizeValue
            color: maintextcolor
            text: qsTr("Телефон:");
            verticalAlignment: Text.AlignVCenter
            font.pointSize: height / 3
        }
        Rectangle {
            height: sizeValue
            width: parent.width
            color: maincolor
            border.color: maintextcolor
            border.width: 1

            TextInput {
                id: telephonRegInput
                anchors.fill: parent
                anchors.leftMargin: 5
                color: maintextcolor
                verticalAlignment: Text.AlignVCenter
                font.pointSize: height / 3
                wrapMode: TextInput.WrapAnywhere

                onAccepted: passwordRegInput.focus = true;
            }
        }

        // password
        Text {
            height: sizeValue
            color: maintextcolor
            text: qsTr("Пароль:");
            verticalAlignment: Text.AlignVCenter
            font.pointSize: height / 3
        }
        Rectangle {
            height: sizeValue
            width: parent.width
            color: maincolor
            border.color: maintextcolor
            border.width: 1

            TextInput {
                id: passwordRegInput
                anchors.fill: parent
                anchors.leftMargin: 5
                color: maintextcolor
                verticalAlignment: Text.AlignVCenter
                font.pointSize: height / 3
                wrapMode: TextInput.WrapAnywhere
                echoMode: TextInput.Password;

                onAccepted: dpasswordRegInput.focus = true;
            }
        }

        // repeat password
        Text {
            height: sizeValue
            color: maintextcolor
            text: qsTr("Повторити пароль:");
            verticalAlignment: Text.AlignVCenter
            font.pointSize: height / 3
        }
        Rectangle {
            height: sizeValue
            width: parent.width
            color: maincolor
            border.color: maintextcolor
            border.width: 1

            TextInput {
                id: dpasswordRegInput
                anchors.fill: parent
                anchors.leftMargin: 5
                color: maintextcolor
                verticalAlignment: Text.AlignVCenter
                font.pointSize: height / 3
                wrapMode: TextInput.WrapAnywhere
                echoMode: TextInput.Password;

                onAccepted: ;//
            }
        }

        Rectangle {
            height: sizeValue
            width: parent.width
            opacity: 0
        }

        // buttons

        Row {
            spacing: 10
            height: sizeValue
            width: parent.width

            Rectangle {
                height: parent.height
                width: (parent.width - parent.spacing) / 2
                color: maintextcolor
                opacity: opacityValue

                Text {
                    anchors.centerIn: parent
                    text: "Відминити"
                    color: maincolor
                    font.pointSize: parent.height / 3
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("cancel reg");//call auth
                        cancel();
                    }
                }
            }

            Rectangle {
                height: parent.height
                width: (parent.width - parent.spacing) / 2
                color: maintextcolor
                opacity: opacityValue

                Text {
                    anchors.centerIn: parent
                    text: "Зарєструвати"
                    color: maincolor
                    font.pointSize: parent.height / 3
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt_server.tryRegistrate(emailRegInput.text, nameRegInput.text, telephonRegInput.text,
                                                       passwordRegInput.text, dpasswordRegInput.text)
                }
            }
        }
    }
}
