import QtQml 2.2
import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

//import minerva.core.account 1.0
//import minerva.core.m_namespace 1.0
import "qrc:/QML"

Item {
    id: mainWindow
    anchors.fill: parent

    // size
    property int size: height/12
    property double opacityValue: 0.75

    // save last open window
    property string lastWindowOpen: "home"

    // color
    property string currentcolor:  "white"
    property string hidecolor: "lightgray"
    property string maincolor: "RoyalBlue"
    property string maintextcolor: "white"

    // date
    property date currentDate: new Date()

    // function
    function addNewPersonalCompanion(name){
        message.addNewPersonalCompanion(name)
    }

    function addNewGroupCompanion(name){
        message.addNewPersonalCompanion(name)
    }

    function clearPersonalCompanion(name){
        message.clearPersonalCompanion()
    }

    function clearGroupCompanion(name){
        message.clearGroupCompanion()
    }

    function hideAll(){
        contacts.visible = false;
        home.visible = false;
        message.visible = false;
        settings.visible = false;
        timeTable.visible = false;
        dialog.visible = false;
        calendar.visible = false;
        dayinfo.visible = false;
        companionlist.visible = false;
        resultWindow.visible = false;

        leftMenu.setHideColorToButton()

        leftMenu.visible = false
    }

    TitlePanel {
        id: topPanel
        height: size
        color: maincolor
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
    }

    Rectangle {
        id: contentFrame
        objectName: "contentFrame"
        color: "white"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: topPanel.bottom

        Contacts {
            id: contacts
            visible: false
        }

        Home {
            id: home
            visible: true
        }

        Messages {
            id: message
            visible: false
        }

        Settings {
            id: settings
            visible: false
        }

        TimeTable {
            id: timeTable
            objectName: "timeTable"
            visible: false
        }

        Dialog {
            id: dialog
            visible: false
        }

        Calendario {
            id: calendar
            visible: false
        }

        DayInfo {
            id: dayinfo
            visible: false
        }

        CompanionList {
            id: companionlist
            visible: false
        }
    }

    LeftMenu {
        id: leftMenu
        color: "lightgray"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: topPanel.bottom
        anchors.bottom: parent.bottom
        visible: false
    }

    ResultWindow {
        id: resultWindow
        objectName: "resultWindow"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: topPanel.bottom
        anchors.bottom: parent.bottom
        visible: false
    }
}
