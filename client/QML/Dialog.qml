import QtQuick 2.0
import QtQuick.Controls 2.0

Rectangle {
    anchors.fill: parent

    TextField {
        id: textInputDialog
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: size
    }
}
