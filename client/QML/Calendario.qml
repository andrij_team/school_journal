import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Calendar {
    anchors.fill: parent
    frameVisible: true
    weekNumbersVisible: true

    onClicked: {
        Qt_server.showDate(date)
    }
}
