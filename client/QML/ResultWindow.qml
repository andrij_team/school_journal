import QtQuick 2.0

Rectangle {
    opacity: opacityValue
    color: maintextcolor
    z: 32555

    function setResult(isAccept, msg){
        if (isAccept)
            resultWindowText.color = "green";
        else
            resultWindowText.color = "red";
        resultWindowText.text = msg;
        resultWindow.visible = true;
    }

    Text {
        width: parent.width * 0.8
        height: parent.height * 0.8
        id: resultWindowText
        anchors.centerIn: parent
        font.pointSize: size / 3
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        anchors.fill: parent
        color: maincolor
        text: "* натисніть по екрану, щоб закрити вікно"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignBottom
        font.pointSize: size / 5
        wrapMode: Text.WordWrap
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            resultWindowText.text = "";
            resultWindow.visible = false;
        }
    }
}
