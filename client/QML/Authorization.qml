import QtQuick 2.0
import "qrc:/QML"

Rectangle {
    property int marginHeight: 5
    property int marginWidth: 10
    signal registrate();
    signal restorePassword();

    onVisibleChanged: {
        if (visible === false)
            return;
        emailAuthInput.focus = true;
        emailAuthInput.clear();
        passwordAuthInput.clear();
        topPanel.setNewTitle("Авторизація");
        console.log("sss auth");
    }

    Column {
        height: parent.height - 2 * marginHeight
        width:  parent.width  - 2 * marginWidth
        x: marginWidth
        y: marginHeight
        spacing: 2

        Text {
            height: parent.height / 5 - 4
            color: maintextcolor
            text: qsTr("Пошта:");
            verticalAlignment: Text.AlignVCenter
            font.pointSize: height / 3
        }
        Rectangle {
            height: parent.height / 5 - 4
            width: parent.width
            color: maincolor
            border.color: maintextcolor
            border.width: 1

            TextInput {
                id: emailAuthInput
                anchors.fill: parent
                anchors.leftMargin: 5
                color: maintextcolor
                verticalAlignment: Text.AlignVCenter
                font.pointSize: height / 3
                focus: true
                wrapMode: TextInput.WrapAnywhere

                onAccepted: passwordAuthInput.focus = true;
            }
        }
        Text {
            height: parent.height / 5 - 4
            color: maintextcolor
            text: qsTr("Пароль:");
            verticalAlignment: Text.AlignVCenter
            font.pointSize: height / 3
        }
        Rectangle {
            id: passwordAuthRect
            height: parent.height / 5 - 4
            width: parent.width
            color: maincolor
            border.color: maintextcolor
            border.width: 1

            TextInput {
                id: passwordAuthInput
                anchors.fill: parent
                anchors.leftMargin: 5
                color: maintextcolor
                verticalAlignment: Text.AlignVCenter
                font.pointSize: height / 3
                focus: true
                wrapMode: TextInput.Wrap
                echoMode: TextInput.Password;

                onAccepted: passwordAuthInput.focus = true;
            }
        }

        Rectangle {
            height: parent.height / 5 + 4*4 - 5 * parent.spacing
            width: parent.width
            color: maincolor

            Text {
                height: parent.height / 2
                width: parent.width
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: 5
                text: "відновити пароль"
                color: maintextcolor
                font.pointSize: height / 2
                font.underline: true
                verticalAlignment: Text.AlignVCenter
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("click restore");
                        restorePassword();
                    }
                }
            }

            Text {
                height: parent.height / 2
                width: parent.width
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: 5
                text: "регістрація"
                color: maintextcolor
                font.pointSize: height / 2
                font.underline: true
                verticalAlignment: Text.AlignVCenter
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("click reg");
                        registrate();
                    }
                }
            }

            Rectangle {
                height: parent.height * 2 / 3
                width: parent.width / 2
                color: maintextcolor
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                opacity: opacityValue

                Text {
                    anchors.centerIn: parent
                    text: "Ввійти"
                    color: maincolor
                    font.pointSize: parent.height / 3
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt_server.tryAuth(emailAuthInput.text, passwordAuthInput.text);
                }
            }
        }
    }
}
