import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {

    function setNewTitle(msg){
        title.text = msg
    }

    function showBackButton(){
        backButton.visible = true
        menuButton.visible = false
    }

    function hideBackButton(){
        backButton.visible = false
        menuButton.visible = true
    }

    Text {
        id: title
        color: maintextcolor
        font.pointSize: height / 4
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "Шкільний журнал"
        anchors.fill: parent
    }

    Rectangle {
        id: menuButton
        width: size
        height: size
        anchors.top: parent.top
        anchors.left: parent.left
        color: parent.color

        Image {
            source: "qrc:/Images/menu.png"
            anchors.centerIn: parent
            sourceSize.width: parent.width*2/3
            sourceSize.height: parent.height*2/3
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                leftMenu.visible = ! leftMenu.visible
            }
        }
    }

    Rectangle {
        id: backButton
        width: size
        height: size
        anchors.top: parent.top
        anchors.left: parent.left
        color: parent.color
        visible: false

        Text {
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            font.pixelSize: parent.height / 2
            text: "<"
            color: maintextcolor
        }

//        Image {
//            source: "qrc:/Images/back.png"
//            sourceSize.width: parent.width*2/3
//            sourceSize.height: parent.height*2/3
//            anchors.centerIn: parent
//        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideAll()
                if (lastWindowOpen == "home"){
                    home.visible = true
                    homeButton.color = currentcolor
                    title.text = "Головна"
                }
                else if (lastWindowOpen == "message"){
                    message.visible = true
                    title.text = "Повідомлення"
                }
                else if (lastWindowOpen == "contacts"){
                    contacts.visible = true
                    title.text = "Контакти"
                }
                else if (lastWindowOpen == "timetable"){
                    timeTable.visible = true
                    title.text = "Розклад"
                }
                else if (lastWindowOpen == "settings"){
                    settings.visible = true
                    title.text = "Налаштування"
                }

                leftMenu.selectCurrent()
                topPanel.hideBackButton()
            }
        }
    }
}
