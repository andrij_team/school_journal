import QtQuick 2.0

Rectangle {
    anchors.fill: parent
    color: maincolor

    ListModel {
        id: listModel

        ListElement {
            namesettings: "Сергій Феленко"
            textsettings: "обрати"
        }

        ListElement {
            namesettings: "Андрій Феленко"
            textsettings: "обраний"
        }
    }

    ListView {
        anchors.centerIn: parent
        height: parent.height - 10
        width: parent.width - 10
        spacing: 5
        model: listModel
        clip: true

        delegate: Rectangle {
            id: listDelegate
            color: maintextcolor

            width: parent.width
            height: size * 2

            Grid {
                anchors.fill: parent
                spacing: 5
                columns: 2

                Text {
                    text: "  І'мя:"
                    height: size
                    width: parent.width / 2 - parent.spacing
                    color: maincolor
                    font.pixelSize: size / 3
                    verticalAlignment: Text.AlignVCenter
                }

                Text {
                    text: namesettings
                    height: size
                    width: parent.width / 2 - parent.spacing
                    color: maincolor
                    font.pixelSize: size / 3
                    verticalAlignment: Text.AlignVCenter
                }

                Rectangle {
                    color: maintextcolor
                    height: size * 2 / 3
                    width: parent.width / 2 - parent.spacing - 10
                }

                Rectangle {
                    height: size * 2 / 3
                    width: parent.width / 2 - parent.spacing - 10
                    color: maincolor

                    Text {
                        anchors.fill: parent
                        text: textsettings
                        color: maintextcolor
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: size / 3
                    }
                }
            }
        }
    }
}
