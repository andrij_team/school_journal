import QtQuick 2.0

Rectangle {
    property int marginHeight: 5
    property int marginWidth: 10
    property int sizeValue: (size * 4 - marginHeight) / 5 - 4
    signal cancel();

    onVisibleChanged: {
        if (visible === false)
            return;
        emailRestoreInput.focus = true;
        emailRestoreInput.clear();
        topPanel.setNewTitle("Відновлення паролю");
    }

    Column {
        height: parent.height - 2 * marginHeight
        width:  parent.width  - 2 * marginWidth
        x: marginWidth
        y: marginHeight
        spacing: 2

        // email
        Text {
            height: sizeValue
            color: maintextcolor
            text: qsTr("Пошта:");
            verticalAlignment: Text.AlignVCenter
            font.pointSize: height / 3
        }
        Rectangle {
            height: sizeValue
            width: parent.width
            color: maincolor
            border.color: maintextcolor
            border.width: 1

            TextInput {
                id: emailRestoreInput
                anchors.fill: parent
                anchors.leftMargin: 5
                color: maintextcolor
                verticalAlignment: Text.AlignVCenter
                font.pointSize: height / 3
                wrapMode: TextInput.WrapAnywhere

                onAccepted: nameRegInput.focus = true;
            }
        }

        Rectangle {
            height: sizeValue
            width: parent.width
            opacity: 0
        }

        // buttons
        Row {
            spacing: 10
            height: sizeValue
            width: parent.width

            Rectangle {
                height: parent.height
                width: (parent.width - parent.spacing) / 2
                color: maintextcolor
                opacity: opacityValue

                Text {
                    anchors.centerIn: parent
                    text: "Відминити"
                    color: maincolor
                    font.pointSize: parent.height / 3
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("cancel reg");//call auth
                        cancel();
                    }
                }
            }

            Rectangle {
                height: parent.height
                width: (parent.width - parent.spacing) / 2
                color: maintextcolor
                opacity: opacityValue

                Text {
                    anchors.centerIn: parent
                    text: "Нагадати"
                    color: maincolor
                    font.pointSize: parent.height / 3
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt_server.tryRestorePassword(emailRestoreInput.text);
                }
            }
        }
    }
}
