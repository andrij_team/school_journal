import QtQuick 2.0

Rectangle {
    anchors.fill: parent
    color: maincolor

    property int marginHeight: 5
    property int marginWidth: 10
    signal logout();

    onVisibleChanged: {
        if (visible === false)
            return;
        topPanel.setNewTitle("Інфо: Біологія");
        console.log("sss auth");
    }

    Grid {
        height: parent.height - 2 * marginHeight
        width:  parent.width  - 2 * marginWidth
        x: marginWidth
        y: marginHeight
        spacing: 5
        columns: 2

        Text {
            text: "Номер:"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "2"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Дата:"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "21.10.2016"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Назва:"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Біологія"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Час:"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "8:55 - 9:40"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Вчитель:"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Нина Ф. Е."
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Оцінки:"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "10"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Попередження:"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 3
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            text: "Погана поведінка"
            height: size
            width: parent.width / 2 - parent.spacing
            color: maintextcolor
            font.pixelSize: size / 4
            verticalAlignment: Text.AlignVCenter
        }
    }
}
