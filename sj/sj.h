#ifndef SJ_H
#define SJ_H

#include <QtCore>

class Sj : public QObject
{
    Q_OBJECT
public:
    Sj() = default;

    enum class Grant {
        Admin = 0,
        Parent = 1,
        Student = 2,
        Teacher = 3,
        Principal = 4,
        Guest = 5,
    };
    Q_ENUM(Grant)

    enum class Request {
        // get
        getLessonByDate = 0x00,
        getAccountInfo = 0x01,

        // set

        // account
        registrate = 0xF0,
        authorizate = 0xF1,
        restorePassword = 0xF2,
    };
    Q_ENUM(Request)

    friend QDataStream& operator << (QDataStream &s, Grant t){
        return s << static_cast<int>(t);
    }

    friend QDataStream& operator >> (QDataStream &s, Grant &t){
        int i;
        s >> i;
        t = static_cast<Sj::Grant>(i);
        return s;
    }

    friend QDataStream& operator << (QDataStream &s, Request r){
        return s << static_cast<int>(r);
    }

    friend QDataStream& operator >> (QDataStream &s, Request &r){
        int i;
        s >> i;
        r = static_cast<Sj::Request>(i);
        return s;
    }
};

#endif // SJ_H
