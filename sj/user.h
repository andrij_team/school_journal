#ifndef USER_H
#define USER_H

#include "sj.h"
#include <QtCore>

class User
{
private:
    bool isLoginValue;
    QString name;
    QString email;
    QString phone;
    Sj::Grant grant;

public:
    User() : isLoginValue(!false){}

    bool      isLogin () { return isLoginValue; }
    QString   getName () { return name;  }
    QString   getEmail() { return email;  }
    QString   getPhone() { return phone; }
    Sj::Grant getGrant() { return grant; }

    void logout() { isLoginValue = false; qDebug() << "set false"; }
    void login()  { isLoginValue = true; qDebug() << "set true"; }

    friend QDataStream& operator << (QDataStream &s, User &u){
        return s << u.name << u.email << u.phone << u.grant;
    }

    friend QDataStream& operator >> (QDataStream &s, User &u){
        return s >> u.name >> u.email >> u.phone >> u.grant;
    }
};

#endif // USER_H
